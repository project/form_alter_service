<?php

namespace Drupal\form_alter_service;

use Drupal\form_alter_service\Annotation\FormHandler;
use Drupal\form_alter_service\Form\FormAlter;
use Drupal\form_alter_service\Form\FormBuilderAlterInterface;
use Drupal\form_alter_service\Annotation\FormSubmit;
use Drupal\form_alter_service\Annotation\FormValidate;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Doctrine\Common\Annotations\Reader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Reflection\Validator\Annotation\ReflectionValidatorAnnotationReader;

/**
 * Registers the form alteration services.
 */
class FormAlterCompilerPass implements CompilerPassInterface {

  /**
   * {@inheritdoc}
   *
   * @example
   * Define the form alteration service.
   * @code
   * form_alter.node_news_form:
   *   class: Drupal\my_module\NodeNewsFormAlter
   *   arguments: ['node_news_form']
   *   tags:
   *     - { name: form_alter, priority: 10 }
   * @endcode
   *
   * @throws \ReflectionException
   *   When the reflection for service class cannot be created.
   * @throws \LogicException
   *   When the error occurs during analyzing the handlers.
   */
  public function process(ContainerBuilder $container): void {
    if ($container->hasDefinition(FormAlter::SERVICE_ID)) {
      $form_builder = $container->getDefinition('form_builder');
      $translation = $container->getDefinition('string_translation');
      $form_alter = $container->getDefinition(FormAlter::SERVICE_ID);
      $services = [];

      foreach ($container->findTaggedServiceIds('form_alter') as $service_id => $tags) {
        $alter = $container
          ->getDefinition($service_id)
          // Set string translation service as it required by the injecting
          // trait (see description by the reference below).
          /* @see \Drupal\Core\StringTranslation\StringTranslationTrait::setStringTranslation() */
          ->addMethodCall('setStringTranslation', [$translation]);

        $locator = $alter->getArgument(0);

        if (empty($locator) || !\is_string($locator)) {
          throw new \InvalidArgumentException(\sprintf('Argument 1 for "%s" service must be a string (form ID, base form ID, or "match" - special keyword to compute operability in runtime).', $service_id));
        }

        foreach ($tags as $attributes) {
          $attributes += ['priority' => 0];
          $services[$locator][] = [$attributes['priority'], $alter];
        }
      }

      if (!empty($services)) {
        foreach ($this->processServices($services) as $locator => $arguments) {
          /* @see \Drupal\form_alter_service\Form\FormAlter::registerService() */
          $form_alter->addMethodCall('registerService', [$locator, $arguments]);
        }
      }

      if (\is_a($form_builder->getClass(), FormBuilderAlterInterface::class, TRUE)) {
        $form_builder->addMethodCall('setFormAlter', [$form_alter]);
      }
    }
  }

  /**
   * Returns the generator of processed form alteration services.
   *
   * @param \Symfony\Component\DependencyInjection\Definition[][] $collection
   *   The collection of registered services.
   *
   * @return \Generator|\Symfony\Component\DependencyInjection\Definition[]
   *   The generator.
   *
   * @throws \ReflectionException
   *   When the reflection for service class cannot be created.
   * @throws \LogicException
   *   When the error occurs during analyzing the handlers.
   */
  protected function processServices(array $collection): \Generator {
    $reader = new ReflectionValidatorAnnotationReader();
    $reader->addNamespace('Drupal\form_alter_service\Annotation');

    // Clear the annotation loaders of any previous annotation classes.
    AnnotationRegistry::reset();
    // Register the namespaces of classes that can be used for annotations.
    AnnotationRegistry::registerLoader('class_exists');

    foreach ($collection as $locator => $services) {
      \array_multisort($collection[$locator], \SORT_ASC);

      foreach ($services as [$priority, $service]) {
        $handlers = $this->getServiceHandlers($service, $reader);

        foreach ($handlers as $type => $strategies) {
          foreach ($strategies as $strategy => $items) {
            \array_multisort($handlers[$type][$strategy], \SORT_ASC);
          }
        }

        yield $locator => $service->addMethodCall('setHandlers', [$handlers]);
      }
    }
  }

  /**
   * Returns handlers for the given form alteration service definition.
   *
   * @param \Symfony\Component\DependencyInjection\Definition $service
   *   The form alteration service definition.
   * @param \Doctrine\Common\Annotations\Reader $reader
   *   The annotation reader.
   *
   * @return array[][][]
   *   The handlers list.
   *
   * @throws \ReflectionException
   *   When the reflection for service class cannot be created.
   * @throws \LogicException
   *   When the error occurs during analyzing the handlers.
   *
   * @see \Drupal\form_alter_service\FormAlterBase::setHandlers()
   */
  protected function getServiceHandlers(Definition $service, Reader $reader): array {
    $handlers = [];
    $errors = [];

    foreach ((new \ReflectionClass($service->getClass()))->getMethods(\ReflectionMethod::IS_PUBLIC) as $method) {
      foreach ([FormValidate::class, FormSubmit::class] as $annotation) {
        try {
          if ($handler = $reader->getMethodAnnotation($method, $annotation)) {
            \assert($handler instanceof FormHandler);
            $handlers[(string) $handler][$handler->strategy][] = [$handler->priority, $method->name];
            // A single method cannot be validation and submission handler in
            // the same time, so break here.
            break;
          }
        }
        catch (\Exception $e) {
          $errors[] = $e->getMessage();
        }
      }
    }

    if (empty($errors)) {
      return $handlers;
    }

    throw new \LogicException(\implode(\PHP_EOL, $errors));
  }

}
