<?php

namespace Drupal\form_alter_service\Form;

/**
 * Allows attaching a `form_alter` service to the `form_builder`.
 *
 * @see \Drupal\form_alter_service\FormAlterServiceServiceProvider
 * @see \Drupal\form_alter_service\FormAlterCompilerPass
 *
 * @ingroup form_api
 */
interface FormBuilderAlterInterface {

  /**
   * Sets the `form_alter` service.
   *
   * @param \Drupal\form_alter_service\Form\FormAlter $form_alter
   *   The `form_alter` service.
   */
  public function setFormAlter(FormAlter $form_alter): void;

}
