<?php

namespace Drupal\form_alter_service\Form;

use Drupal\Core\Form\FormBuilder as FormBuilderBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * The `form_builder` service override.
 */
class FormBuilder extends FormBuilderBase implements FormBuilderAlterInterface {

  /**
   * An instance of the `form_alter` service.
   *
   * @var \Drupal\form_alter_service\Form\FormAlter
   */
  protected $formAlter;

  /**
   * {@inheritdoc}
   */
  public function setFormAlter(FormAlter $form_alter): void {
    $this->formAlter = $form_alter;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareForm($form_id, &$form, FormStateInterface &$form_state): void {
    parent::prepareForm($form_id, $form, $form_state);

    $build_info = $form_state->getBuildInfo();
    $arguments = $build_info['args'] ?? [];
    \assert(\is_array($arguments), $form_id);
    // We preserve the ordering from the core so initially call all
    // alters, then `base_form_id` and `$form_id`. See the last lines
    // in the parent method.
    $alters = $this->formAlter->getServices('match');

    if (isset($build_info['base_form_id'])) {
      $alters = \array_merge($alters, $this->formAlter->getServices($build_info['base_form_id']));
    }

    $alters = \array_merge($alters, $this->formAlter->getServices($form_id));

    foreach ($alters as $alter) {
      /* @var \Drupal\form_alter_service\FormAlterBase $alter */
      if ($alter->hasMatch($form, $form_state, $form_id)) {
        // Expand the arguments as they passing to the `buildForm()` method.
        $alter->alterForm($form, $form_state, ...$arguments);

        foreach ($alter->getHandlers() as $type => $handlers) {
          // Make sure the array is initialized to prevent cases like
          // `array_unshift() expects parameter 1 to be array, null given`.
          $form += [$type => []];

          foreach ($handlers['prepend'] ?? [] as [$priority, $handler]) {
            \array_unshift($form[$type], [$alter, $handler]);
          }

          foreach ($handlers['append'] ?? [] as [$priority, $handler]) {
            $form[$type][] = [$alter, $handler];
          }
        }
      }
    }
  }

}
