<?php

namespace Drupal\form_alter_service\Form;

use Drupal\form_alter_service\FormAlterBase;

/**
 * The container for form alteration services.
 *
 * @ingroup form_api
 */
class FormAlter {

  /**
   * The service ID.
   */
  public const SERVICE_ID = 'form_alter';

  /**
   * The form alteration services.
   *
   * @var \Drupal\form_alter_service\FormAlterBase[][]
   */
  protected $services = [];

  /**
   * Registers the form alteration service.
   *
   * @param string $id
   *   The form ID or base form ID.
   * @param \Drupal\form_alter_service\FormAlterBase $service
   *   The form alteration service.
   */
  public function registerService(string $id, FormAlterBase $service): void {
    $this->services[$id][] = $service;
  }

  /**
   * Returns the list of form alteration services.
   *
   * @param string $id
   *   The form ID or base form ID.
   *
   * @return \Drupal\form_alter_service\FormAlterBase[]
   *   The list of form alteration services.
   */
  public function getServices(string $id): array {
    return $this->services[$id] ?? [];
  }

}
