<?php

namespace Drupal\form_alter_service\Annotation;

/**
 * The annotation for defining a form submit handlers.
 *
 * @Annotation
 * @Target({"METHOD"})
 */
class FormSubmit extends FormHandler {

  /**
   * {@inheritdoc}
   */
  public function __toString(): string {
    return '#submit';
  }

}
