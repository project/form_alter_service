<?php

namespace Drupal\form_alter_service\Annotation;

use Drupal\Core\Form\FormStateInterface;
use Drupal\form_alter_service\FormAlterBase;
use Reflection\Validator\MethodValidator;
use Reflection\Validator\ArgumentSpecification;
use Reflection\Validator\Annotation\ReflectionValidatorMethodAnnotationInterface;

/**
 * The base annotation for defining a form handler.
 */
abstract class FormHandler implements ReflectionValidatorMethodAnnotationInterface {

  /**
   * The handler priority.
   *
   * @var int
   *
   * @Assert\Type("int")
   */
  public $priority = 0;

  /**
   * The handler strategy.
   *
   * @var string
   *
   * @Enum({"append", "prepend"})
   */
  public $strategy = 'append';

  /**
   * Returns the type of handler ("#submit" or "#validate").
   *
   * @return string
   *   The type of handler.
   */
  abstract public function __toString(): string;

  /**
   * {@inheritdoc}
   */
  public function validate(\ReflectionMethod $method): void {
    (new MethodValidator($method, FormAlterBase::class))
      ->addArgument(
        (new ArgumentSpecification('form'))
          ->setType('array')
          ->setOptional(FALSE)
          // The "$form" argument for validation handlers must be passed by
          // reference. For submit - no way.
          ->setPassedByReference(is_a(static::class, FormValidate::class, TRUE))
      )
      ->addArgument(
        (new ArgumentSpecification('form_state'))
          ->setType(FormStateInterface::class)
          ->setOptional(FALSE)
          ->setPassedByReference(FALSE)
      );
  }

}
