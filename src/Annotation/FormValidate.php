<?php

namespace Drupal\form_alter_service\Annotation;

/**
 * The annotation for defining a form validation handlers.
 *
 * @Annotation
 * @Target({"METHOD"})
 */
class FormValidate extends FormHandler {

  /**
   * {@inheritdoc}
   */
  public function __toString(): string {
    return '#validate';
  }

}
