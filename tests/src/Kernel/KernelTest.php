<?php

namespace Drupal\Tests\form_alter_service\Kernel;

use Drupal\form_alter_service_test\NodeFormAlterTest;
use Drupal\form_alter_service_test\NodeFormIdAlterTest;
use Drupal\form_alter_service_test\NodeFormMatchAlterTest;
use Drupal\KernelTests\KernelTestBase;
use Drupal\form_alter_service\Form\FormAlter;

/**
 * Tests the module functionality.
 *
 * @group form_alter_service
 */
class KernelTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'form_alter_service',
    'form_alter_service_test',
  ];

  /**
   * Tests that ordering of handlers execution is appropriate.
   *
   * @throws \Exception
   */
  public function testHandlersPrioritisation(): void {
    $form_alter = $this->container->get(FormAlter::SERVICE_ID);
    static::assertTrue(\assert($form_alter instanceof FormAlter));
    $services = new \ReflectionProperty($form_alter, 'services');
    $services->setAccessible(TRUE);

    static::assertCount(4, $services->getValue($form_alter));
    static::assertCount(2, $form_alter->getServices('node_form'));
    static::assertCount(1, $form_alter->getServices('match'));
    static::assertCount(1, $form_alter->getServices('node_page_edit_form'));
    static::assertCount(1, $form_alter->getServices('views_form_user_admin_people_page_1'));

    $handlers = $this->container->get('form_alter.node_form')->getHandlers();

    static::assertNotEmpty($handlers['#validate']);
    static::assertNotEmpty($handlers['#submit']);
    static::assertSame([
      [-5, 'validateThird'],
      [0, 'validateSecond'],
      [10, 'validateFirst'],
    ], $handlers['#validate']['prepend']);
  }

  /**
   * Tests that `hasMatch()` method can block `alterForm()` from being called.
   *
   * @throws \Exception
   */
  public function testHasMatchAndHandlersPopulation(): void {
    $this->enableModules(['user', 'node', 'system']);
    $this->installConfig(['system']);
    $this->installSchema('system', ['sequences']);

    foreach ([
      'node' => ['node_access'],
      'user' => ['users_data'],
    ] as $module => $tables) {
      $this->installEntitySchema($module);
      $this->installSchema($module, $tables);
    }

    $form_alter = $this->container->get(FormAlter::SERVICE_ID);
    static::assertTrue(\assert($form_alter instanceof FormAlter));

    $property = new \ReflectionProperty($form_alter, 'services');
    $property->setAccessible(TRUE);
    $services = $property->getValue($form_alter);

    foreach (['node_form', 'node_page_edit_form', 'match'] as $locator) {
      static::assertTrue(isset($services[$locator]), $locator);

      foreach ($form_alter->getServices($locator) as $i => $service) {
        static::assertSame($services[$locator][$i], $service, "$locator:$i");

        $mock = $this
          ->getMockBuilder(get_class($service))
          ->setConstructorArgs([$locator])
          ->setMethods(['hasMatch'])
          ->getMock();

        $mock
          ->expects(static::once())
          ->method('hasMatch')
          ->willReturn(TRUE);

        // Copy handlers into mocked service since `setHandlers()` method is
        // final and cannot be mocked.
        $mock->setHandlers($service->getHandlers());
        // Replace with the mock.
        $services[$locator][$i] = $mock;
      }
    }

    $property->setValue($form_alter, $services);

    $this->container
      ->get('form_builder')
      // Update the form alter with mocked services.
      /* @see \Drupal\form_alter_service\Form\FormBuilder::setFormAlter() */
      ->setFormAlter($form_alter);

    $entity_manager = $this->container->get('entity_type.manager');
    $node_type_storage = $entity_manager->getStorage('node_type');
    $user_storage = $entity_manager->getStorage('user');
    $node_storage = $entity_manager->getStorage('node');

    $node_type = $node_type_storage->create([
      'name' => 'Page',
      'type' => 'page',
    ]);

    $node_type_storage->save($node_type);

    $user = $user_storage->create([
      'name' => $this->randomString(),
    ]);

    $user_storage->save($user);

    $node = $node_storage->create([
      'uid' => $user->id(),
      'type' => $node_type->id(),
      'title' => 'Test',
    ]);

    $node_storage->save($node);

    $form = $this->container
      ->get('entity.form_builder')
      ->getForm($node, 'edit');

    // Form alters should be invoked in a correct order.
    static::assertSame(NodeFormMatchAlterTest::HOOK_ID, $form['execution_order'][0]['#markup']);
    static::assertSame(NodeFormAlterTest::HOOK_ID, $form['execution_order'][1]['#markup']);
    static::assertSame(NodeFormIdAlterTest::HOOK_ID, $form['execution_order'][2]['#markup']);

    /* @see \Drupal\form_alter_service_test\NodeFormAlterTest::alterForm() */
    static::assertSame('', $form['markup']['#markup']);
    /* @see \Drupal\form_alter_service_test\NodeFormAlter2Test::alterForm() */
    static::assertSame('NOBODY CAN STOP ME!', $form['dummy']['#markup']);
    static::assertCount(4, $form['#validate']);
    static::assertCount(2, $form['#submit']);
  }

}
