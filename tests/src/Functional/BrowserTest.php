<?php

namespace Drupal\Tests\form_alter_service\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the modifications made by form alteration services.
 *
 * @group form_alter_service
 */
class BrowserTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'form_alter_service_test',
    'views',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * An instance of the `string_translation` service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $translation;

  /**
   * The assertion service.
   *
   * @var \Drupal\Tests\WebAssert
   */
  protected $assert;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalLogin($this->rootUser);
    $this->translation = $this->container->get('string_translation');
    $this->assert = $this->assertSession();
  }

  /**
   * Tests that arguments from build info passed to the `alterForm()` method.
   *
   * @throws \Exception
   *
   * @see \Drupal\form_alter_service_test\UsersListFormAlterTest::alterForm()
   */
  public function testArgumentsExpansion(): void {
    $this->drupalGet('admin/people');
    $this->assert->statusCodeEquals(200);
    $this->assert->responseContains($this->translation->translate('Test title 2'));
  }

  /**
   * Tests that ordering of handlers execution is appropriate.
   *
   * @throws \Exception
   */
  public function testHandlersPrioritisation(): void {
    $this->drupalCreateContentType([
      'type' => 'page',
      'name' => 'Basic page',
      'display_submitted' => FALSE,
    ]);

    $this->drupalPostForm(
      'node/' . $this->drupalCreateNode()->id() . '/edit',
      ['title[0][value]' => 'Test'],
      $this->translation->translate('Save')
    );

    /* @see \Drupal\form_alter_service_test\NodeFormAlterTest::alterForm() */
    $this->assert->responseContains('validate1:validateFirst|validate2:validateSecond|validate3:validateThird');
    // The `hasMatch()` returns "FALSE" so we must not see it.
    /* @see \Drupal\form_alter_service_test\NodeFormAlter2Test::alterForm() */
    $this->assert->responseNotContains('NOBODY CAN STOP ME!');
  }

}
