<?php

namespace Drupal\form_alter_service_test;

use Drupal\Core\Form\FormStateInterface;
use Drupal\form_alter_service\FormAlterBase;

/**
 * Test form alter.
 */
class NodeFormIdAlterTest extends FormAlterBase {

  /**
   * The hook ID.
   */
  public const HOOK_ID = 'hook_form_form_id_alter';

  /**
   * {@inheritdoc}
   */
  public function alterForm(array &$form, FormStateInterface $form_state): void {
    $form['execution_order'][] = [
      '#markup' => static::HOOK_ID,
    ];
  }

}
