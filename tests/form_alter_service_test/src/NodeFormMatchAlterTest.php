<?php

namespace Drupal\form_alter_service_test;

use Drupal\Core\Form\FormStateInterface;
use Drupal\form_alter_service\FormAlterBase;

/**
 * Test form alter.
 */
class NodeFormMatchAlterTest extends FormAlterBase {

  /**
   * The hook ID.
   */
  public const HOOK_ID = 'hook_form_alter';

  /**
   * {@inheritdoc}
   */
  public function hasMatch(array $form, FormStateInterface $form_state, string $form_id): bool {
    return \strpos($form_id, 'node_page') === 0;
  }

  /**
   * {@inheritdoc}
   */
  public function alterForm(array &$form, FormStateInterface $form_state): void {
    $form['execution_order'][] = [
      '#markup' => static::HOOK_ID,
    ];
  }

}
