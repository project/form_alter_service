<?php

namespace Drupal\form_alter_service_test;

use Drupal\Core\Form\FormStateInterface;
use Drupal\form_alter_service\FormAlterBase;

/**
 * Test form alter.
 */
class NodeFormAlterTest extends FormAlterBase {

  /**
   * The hook ID.
   */
  public const HOOK_ID = 'hook_form_base_form_id_alter';

  /**
   * {@inheritdoc}
   */
  public function alterForm(array &$form, FormStateInterface $form_state): void {
    $markup = [];

    foreach ($form_state->getTemporary() as $key => $value) {
      $markup[] = "$key:$value";
    }

    $form['markup'] = [
      '#markup' => \implode('|', $markup),
    ];

    $form['execution_order'][] = [
      '#markup' => static::HOOK_ID,
    ];

    /* @see \Drupal\Tests\form_alter_service\Functional\BrowserTest::testHandlersPrioritisation() */
    $form['actions']['submit']['#submit'][] = [$this, 'submitButton'];
  }

  /**
   * {@inheritdoc}
   *
   * @FormValidate(
   *   priority = 0,
   *   strategy = "prepend",
   * )
   */
  public function validateSecond(array &$form, FormStateInterface $form_state): void {
    $form_state->setTemporaryValue('validate2', __FUNCTION__);
  }

  /**
   * {@inheritdoc}
   *
   * @FormValidate(
   *   priority = -5,
   *   strategy = "prepend",
   * )
   */
  public function validateThird(array &$form, FormStateInterface $form_state): void {
    $form_state->setTemporaryValue('validate3', __FUNCTION__);
  }

  /**
   * {@inheritdoc}
   *
   * @FormValidate(
   *   priority = 10,
   *   strategy = "prepend",
   * )
   */
  public function validateFirst(array &$form, FormStateInterface $form_state): void {
    $form_state->setTemporaryValue('validate1', __FUNCTION__);
  }

  /**
   * {@inheritdoc}
   *
   * @FormSubmit(
   *   strategy = "prepend",
   * )
   */
  public function submitTest(array $form, FormStateInterface $form_state): void {
    // This method will be skipped because the form has a submit button
    // with the `#submit` property.
    /* @see \Drupal\Core\Form\FormBuilder::doBuildForm() */
    $form_state->setTemporaryValue('submitSkip1', __FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function submitButton(array $form, FormStateInterface $form_state): void {
    $form_state->setRebuild();
  }

}
